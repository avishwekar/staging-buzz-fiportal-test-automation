package com.buzzpoints.selenium.services;

/**
 * Created by mlittman on 11/23/15.
 */
@Deprecated
public class FileManager {

    public enum Type {
        DEFAULT, AWS
    }


    Type managerType = Type.DEFAULT;

    public FileManager(){
        if(Boolean.valueOf((String)Settings.resolve("UseAWS"))) {
            managerType = Type.AWS;
        }
    }

    public static FileManager fileManager = new FileManager();

    public static FileManager getInstance() {
        return fileManager;
    }
}
