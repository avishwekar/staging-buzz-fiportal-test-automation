package com.buzzpoints.selenium.services;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Created by mlittman on 11/23/15.
 */
@Deprecated
public class Settings {

    final static String PROPERTIES_FILE = "config.properties";

    final private static Properties properties = getProperties();

    /**
     * Load our properties file
     * @return a Properties object we have found
     */
    static Properties getProperties () {
        try {
            InputStream inputStream = Settings.class.getClassLoader().getResourceAsStream(PROPERTIES_FILE);
            if (inputStream != null) {
                Properties p = new Properties();
                p.load(inputStream);
                return p;
            } else
                throw new FileNotFoundException("Property file '" + PROPERTIES_FILE + "' not found in the class path.");
        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());

        } catch (IOException e) {
            System.out.println("Could not load file. I/O error.");
        }
        return null;
    }

    /**
     * Attempt to resolve a key it's paired value
     * @param key String key we are searching
     * @return Object we found, else null if not found
     */
    public static Object resolve (String key){
        if(properties != null)
            return properties.get(key);
        return null;
    }
}
