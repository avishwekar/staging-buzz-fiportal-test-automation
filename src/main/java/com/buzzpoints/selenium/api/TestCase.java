package com.buzzpoints.selenium.api;

import com.buzzpoints.selenium.main.BuzzTestAutomation;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;
import com.thoughtworks.selenium.webdriven.JavascriptLibrary;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.io.FileUtils;
import org.joda.time.Duration;
import org.joda.time.format.PeriodFormatter;
import org.joda.time.format.PeriodFormatterBuilder;
import org.openqa.selenium.*;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.apache.logging.log4j.Logger;
import ru.yandex.qatools.ashot.AShot;
import ru.yandex.qatools.ashot.screentaker.ViewportPastingStrategy;

import java.io.File;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * Created by mlittman on 11/9/15.
 */
@Deprecated
public abstract class TestCase {

    protected static final AShot screenshotPage = new AShot().shootingStrategy(new ViewportPastingStrategy(10));

    protected static final JavascriptLibrary jsLib = new JavascriptLibrary();

    protected static final SimpleDateFormat timestampFactory = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss");

    @Getter protected List<String> requiredConfig = new ArrayList<String>();

    protected String caseTitle = "Anonymous Test Case";
    protected String caseDesc  = "";

    /**
     * Convert TestCase to a string using title and description
     * @return
     */
    public String toString () {
        StringBuilder output = new StringBuilder();
        output.append(caseTitle);
        if(!caseDesc.isEmpty())
            output.append(" - ").append(caseDesc);
        return output.toString();
    }

    /**
     * Will grab the configuration
     * @param cfg
     * @param key
     * @return
     */
    public Object getConfig(TestConfig cfg, String key){
        requires(key);
        if(cfg.containsKey(key))
            return cfg.get(key);
        return null;
    }

    /**
     * Sets the title and description of this test case
     * @param title
     * @param description
     */
    public TestCase describe(String title, String description){
        caseTitle = title;
        caseDesc = description;
        return this;
    }

    /**
     * Returns a boolean which represents if we have met our conditions on matching the specified URL pattern.
     * @param driver    the WebDriver we are currently using
     * @param match     the URL pattern we are matching against
     * @param retrys    the maximum number of retries we will attempt
     * @param wait      the delay between each retry
     * @return True if the URL matches before the retry count runs out, else false
     * @throws InterruptedException
     */
    public static boolean waitUntilURLContains(WebDriver driver, String match, int retrys, long wait) throws InterruptedException {
        if(driver == null || match == null)
            return false;

        while(!driver.getCurrentUrl().contains(match) && --retrys > 0)
            Thread.sleep(wait);

        if(retrys <= 0)
            return false;
        return true;
    }

    /**
     * Take a screenshot of the current viewport
     * @param driver the WebDriver currently used
     * @return a File containing the screenshot
     */
    public static File screenshotWindow(WebDriver driver) {
        if(driver == null)
            return null;

        TakesScreenshot screenshotDriver = (TakesScreenshot)driver;
        File screenshotFile = screenshotDriver.getScreenshotAs(OutputType.FILE);
        try{
            File outputFile = new File("output/reports/images/" + generateScreenshotFilename());
            FileUtils.copyFile(screenshotFile, outputFile);
            return outputFile;
        }
        catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Creates a filename for a screenshot
     * @return a String filename
     */
    private static String generateScreenshotFilename() {
        StringBuilder fileName = new StringBuilder();
        fileName.append("pageScreenshot.").append(timestampFactory.format(new Date()));
        fileName.append(".png");
        return fileName.toString();
    }

    /**
     * A safe way to locate an element on the page using a try/catch block
     * @param driver    the WebDriver instance used
     * @param selector  the By selector used
     * @return a WebElement if the element is found, else null
     */
    public WebElement findElement(WebDriver driver, By selector) {
        if(driver == null || selector == null)
            return null;

        try {
            return driver.findElement(selector);
        }
        catch(NoSuchElementException e){
            return null;
        }
    }

    /**
     * Will attempt a physical click on a WebElement inside a current WebDriver
     * @param driver    the current WebDriver instance
     * @param element   the WebElement we are attempting to click
     */
    public void mouseClick(WebDriver driver, WebElement element) {
        if(driver == null || element == null)
            return;

        jsLib.callEmbeddedSelenium(driver, "triggerMouseEventAt", element, "click", "0,0");
    }

    /**
     * Will wait until a certain element exists inside a WebDriver, searches using By selectors
     * @param driver    the current WebDriver instance
     * @param selector  the By selector we are searching for
     * @param timeout   the number of seconds until this request times out
     * @param sleep     the number of milliseconds between each poll
     * @return the WebElement we have found
     */
    public WebElement waitUntilExists(WebDriver driver, By selector, long timeout, long sleep){
        try {
            return (new WebDriverWait(driver, timeout, sleep)).until(ExpectedConditions.visibilityOfElementLocated(selector));
        }
        catch(TimeoutException e){
            return null;
        }
        catch(Exception e){
            log().error("Unexpected error occurred while waiting for element to exist.");
            return null;
        }
    }

    /**
     * Will wait until elements exist inside a WebDriver, searches using By selectors
     * @param driver    the current WebDriver instance
     * @param selector  the By selector we are searching for
     * @param timeout   the number of seconds until this request times out
     * @param sleep     the number of milliseconds between each poll
     * @return the list of WebElement we have found
     */
    public List<WebElement> waitUntilAllExist(WebDriver driver, By selector, long timeout, long sleep){
        try {
            return (new WebDriverWait(driver, timeout, sleep)).until(ExpectedConditions.visibilityOfAllElementsLocatedBy(selector));
        }
        catch(TimeoutException e){
            return null;
        }
        catch(Exception e){
            log().error("Unexpected error occurred while waiting for elements to exist.");
            return null;
        }
    }
    
    /**
     * Will wait until a certain element is no longer inside a WebDriver, searches using By selectors
     * @param driver    the current WebDriver instance
     * @param selector  the By selector we are searching for
     * @param timeout   the number of seconds until this request times out
     * @param sleep     the number of milliseconds between each poll
     * @return the WebElement we have found
     */    
    public Boolean waitUntilNotExists(WebDriver driver, By selector, long timeout, long sleep){
        try {
            return (new WebDriverWait(driver, timeout, sleep)).until(ExpectedConditions.invisibilityOfElementLocated(selector));
        }
        catch(TimeoutException e){
            return null;
        }
        catch(Exception e){
            log().error("Unexpected error occurred while waiting for element to exist.");
            return null;
        }
    }
    
    /**
     * Default error event when a problem occurs within the test case
     * @param resultList the list we are appending to
     * @param error the Exception thrown if present
     * @param driver the WebDriver we are accessing
     */
    public void onError (List<Result> resultList, Exception error, WebDriver driver, File screenshot) {
        Result result = new Result();
        result.setStatus(false);
        result.setException(error);
        result.setScreenshot(screenshot);
        resultList.add(result);
    }

    /**
     * Default success event when a unit test passes
     */
    public void onSuccess(List<Result> resultList) {
        Result result = new Result();
        result.setStatus(true);
        resultList.add(result);
    }


    /**
     * Defines a config variable that we need to execute
     * @param variable the key in config we require
     * @return this
     */
    public TestCase requires(String variable){
        if(!requiredConfig.contains(variable))
            requiredConfig.add(variable);
        return this;
    }

    /**
     * Defines a list of config variables that we need to execute
     * @param variables List of config keys we require
     * @return this
     */
    public TestCase requires(List<String> variables){
        for(String variable : variables)
            requires(variable);
        return this;
    }

    /**
     * Run the test instance
     * @param driver the current WebDriver instance
     * @return True if the test passes, else false
     * @throws InterruptedException
     */
    public abstract Results run (WebDriver driver) throws InterruptedException;

    /**
     * Grab the static logger instance
     * @return the Log4J logger instance
     */
    public static Logger log(){
        return BuzzTestAutomation.logger;
    }

    /**
     * Manages individual results
     */
    public class Result {
        @Getter @Setter(AccessLevel.PROTECTED) private Boolean status;
        @Getter @Setter(AccessLevel.PROTECTED) private Exception exception;
        @Getter @Setter(AccessLevel.PROTECTED) private File screenshot;
        @Getter final private Instant time = Instant.now();

        public String toString() {
            String exceptStr = "";
            if(exception != null)
                return exception.getMessage();
            return String.format("Result; Status: %B, Exception: %s", status, exceptStr);
        }
    }

    /**
     * Manages all results
     */
    public class Results {
        @Getter final private Instant start = Instant.now();
        @Getter private Instant end;
        @Getter private List<Result> resultList;
        @Getter private boolean finalResult;
        @Getter private final TestCase testCase;
        private final WebDriver webDriver;
        private ExtentTest extentTest;

        private final PeriodFormatter periodFormatter = new PeriodFormatterBuilder()
                .appendHours()
                .appendSuffix("h ")
                .appendMinutes()
                .appendSuffix("m ")
                .appendSeconds()
                .appendSuffix("s ")
                .appendMillis()
                .appendSuffix("ms ")
                .toFormatter();

        public Results (TestCase test, WebDriver driver, List<Result> results) {
            testCase = test;
            resultList = results;
            webDriver = driver;
            extentTest = TestSuite.getReporter().startTest(getBrowser() + " / " + testCase.caseTitle);
        }

        /**
         * Attempt to grab a clean browser string
         * @return String representing the browser
         */
        private String getBrowser () {
            if(webDriver instanceof RemoteWebDriver){
                Capabilities caps = ((RemoteWebDriver) webDriver).getCapabilities();
                return caps.getCapability("browserName") + " " + caps.getCapability("version");
            }
            else
                return webDriver.getClass().getSimpleName();
        }

        /**
         * Log to the extent test
         * @param status The LogStatus status of this log
         * @param e Exception if present
         * @param message String message
         */
        private void log(LogStatus status, Exception e, String message){
            if(e == null)
                e = new Exception(message);

            File screenshot = testCase.screenshotWindow(webDriver);

            extentTest.log(status, message);
            extentTest.log(LogStatus.INFO, "Screenshot: " + extentTest.addScreenCapture(screenshot.getName()));

            TestCase.log().error(message);
            testCase.onError(this.resultList, e, webDriver, screenshot);
        }


        /**
         * Produce an error log
         * @param e Exception object
         * @param m String message
         */
        public void error(Exception e, String m){
            log(LogStatus.ERROR, e, m);
        }

        /**
         * Produce a warning log
         * @param e Exception object
         * @param m String message
         */
        public void warn(Exception e, String m){
            log(LogStatus.WARNING, e, m);
        }

        /**
         * Called when the TestCase completes. Records time and overall result
         * @param result boolean result returned
         * @return this
         */
        public Results done (boolean result) {
            finalResult = result;
            end = Instant.now();
            if(result)
                extentTest.log(LogStatus.PASS, "Test succeeded!");
            else
                extentTest.log(LogStatus.FAIL, "Test failed!");
            TestSuite.getReporter().endTest(extentTest);
            return this;
        }

        /**
         * Returns the duration of this result set in milliseconds
         * @return a Long representing how long this result list took to generate, null if not yet finished
         */
        public Long getDuration(){
            if(end == null)
                return null;
            return java.time.Duration.between(start, end).toMillis();
        }

        /**
         *
         * @return
         */
        @Override
        public String toString () {
            long durationMillis = java.time.Duration.between(start, end).toMillis();

            StringBuilder output = new StringBuilder();
            output.append(String.format("Test Case : %s | Final result %b | ", testCase, finalResult));
            output.append("Test took ").append(periodFormatter.print(new Duration(durationMillis).toPeriod()));
            for(Result r : resultList)
                output.append("\n\t").append(r.toString());

            return output.toString();
        }
    }
}
