package com.buzzpoints.selenium.test.app.merchantportal.cases;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.buzzpoints.selenium.api.TestCase;
import com.buzzpoints.selenium.api.TestConfig;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mlittman on 11/9/15.
 */
@Deprecated
public class CaseLogin extends TestCase {
    final String host;
    final String username;
    final String password;
    final String merchant;

    /**
     * Will attempt to login to the merchant portal
     * @param cfg a TestConfig object containing relevant configuration data
     */
    public CaseLogin(TestConfig cfg) {
        host = (String) getConfig(cfg, "host");
        username = (String) getConfig(cfg, "username");
        password = (String) getConfig(cfg, "password");
        merchant = (String) getConfig(cfg, "merchant");
    }

    /**
     * Run the Merchant Portal login test
     * @param driver the current WebDriver instance
     * @return a Results object containing all the results
     * @throws InterruptedException
     */
    public Results run (WebDriver driver) throws InterruptedException{
        List<Result> resultList = new ArrayList<Result>();
        Results results = new Results(this, driver, resultList);

        driver.get(host);

        waitUntilExists(driver, By.id("signin"), 10l, 500l);

        WebElement loginField = driver.findElement(By.id("signinEmail"));
        WebElement passwordField = driver.findElement(By.id("signinPassword"));

        loginField.sendKeys(username);
        passwordField.sendKeys(password);

        passwordField.submit();


        // Wait until the session page loads
        if(waitUntilURLContains(driver, "#/session", 10, 500)){
            List<WebElement> businessList = driver.findElements(By.className("session-picker-item"));
            WebElement targetBusiness = null;
            for(WebElement element : businessList) {
                try{
                    WebElement name = element.findElement(By.tagName("h4"));
                    if(name.getText().equalsIgnoreCase(merchant)){
                        targetBusiness = element;
                        break;
                    }
                }
                catch(Exception e){
                    results.error(e, "Could not locate merchant by name '"+merchant+"'.");
                }
            }

            if(targetBusiness != null){
                mouseClick(driver, targetBusiness);

                if(waitUntilURLContains(driver, "#/fbprompt", 3, 500)){
                    try {
                        WebElement element = driver.findElement(By.xpath("//button[contains(.,'Skip')]"));
                        element.click();
                    }
                    catch(Exception e){
                        results.error(e, "Could not locate 'Skip' button on Facebook prompt.");
                    }
                }
                else{
                    results.warn(null, "Failed to navigate to Facebook prompt. (Maybe did not prompt).");
                }

                if(waitUntilURLContains(driver, "#/portal/summary", 10, 500)){
                    onSuccess(resultList);
                    return results.done(true);
                }
                else {
                    results.error(null, "Failed to navigate to summary page.");
                }
            }
            else {
                results.error(null, "Failed to locate specified session.");
            }
        }
        else {
            results.error(null, "Failed to select a session.");
        }

        return results.done(false);
    }
}
