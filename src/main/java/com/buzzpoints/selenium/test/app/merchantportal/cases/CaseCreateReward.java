package com.buzzpoints.selenium.test.app.merchantportal.cases;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.buzzpoints.selenium.api.TestCase;
import com.buzzpoints.selenium.api.TestConfig;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mlittman on 11/10/15.
 */
@Deprecated
public class CaseCreateReward extends TestCase {
//	public static final Logger logger = LogManager.getLogger();
	final int amount;
	final int otherAmount;

	/**
	 * Creates a CreateReward
	 * @param cfg
	 */
	public CaseCreateReward(TestConfig cfg) {
		amount = (Integer) getConfig(cfg, "reward-amount");
		otherAmount = (Integer) getConfig(cfg, "reward-amount-other");
	}

	/**
	 * Run the Create Reward test case
	 * @param driver the current WebDriver instance
	 * @return True if the reward was created
	 */
	public Results run(WebDriver driver) {
		List<Result> resultList = new ArrayList<Result>();
		Results results = new Results(this, driver, resultList);
		//		int rewardAmount = amount;

		try {
			WebElement navbar = waitUntilExists(driver, By.cssSelector("nav.navbar"), 10l, 500l);
			if (navbar == null)
				throw new Exception("Could not locate the nav bar.");

			WebElement campaignDropdown = navbar.findElement(By.xpath("//a[contains(.,'Campaigns')]"));
			if (campaignDropdown == null)
				throw new Exception("Could not locate Campaigns dropdown inside the nav bar.");
			mouseClick(driver, campaignDropdown);

			WebElement rewardsLink = navbar.findElement(By.cssSelector("a[href=\"#/portal/campaigns/rewards\"]"));
			if (rewardsLink == null)
				throw new Exception("Could not locate Rewards link inside the Campaigns dropdown.");
			rewardsLink.click();

			if (waitUntilURLContains(driver, "#/portal/campaigns/rewards", 10, 500) == false)
				throw new Exception("Could not navigate to Rewards page.");

			WebElement createRewardButton = waitUntilExists(driver, By.xpath("//a[contains(.,'Create a Reward')] | //button[contains(.,'Create a Reward')]"), 20, 250);
			if (createRewardButton == null)
				throw new Exception("Could not find Create a Reward button.");
			mouseClick(driver, createRewardButton);

			WebElement rewardButton = findElement(driver, By.cssSelector("input[ng-value=\"" + String.valueOf(amount) + "\"]"));
			if (rewardButton == null)
				rewardButton = findElement(driver, By.cssSelector("input[ng-value=\"" + String.valueOf(otherAmount) + "\"]"));
			if (rewardButton == null)
				throw new Exception("Could not locate reward button matching amount '" + String.valueOf(otherAmount) + "'.");
			rewardButton.click();

			WebElement nextButton = waitUntilExists(driver, By.xpath("//button[contains(.,'Next')]"), 5, 150);
			if (nextButton == null)
				throw new Exception("Could not find next button.");
			mouseClick(driver, nextButton);

			WebElement createButton = waitUntilExists(driver, By.xpath("//button[contains(.,'Create')]"), 5, 150);
			if (createButton == null)
				throw new Exception("Could not find create button.");
			mouseClick(driver, createButton);
			
			WebElement doneButton = waitUntilExists(driver, By.xpath("//button[contains(.,'Done')]"), 15, 150);
			if (doneButton == null)
				throw new Exception("Could not find done button.");
			mouseClick(driver, doneButton);

			//			Clean up by ending the created reward

			WebElement rewardSpan = waitUntilExists(driver, By.xpath("//span[contains(.,'$5.00')]"), 15, 150);
			if (rewardSpan == null)
				throw new Exception("Cannot find $5 Reward.");
			System.out.println("Found the $5 reward");

            List<WebElement> endRewardButtons = waitUntilAllExist(driver, By.xpath("//button[contains(.,'End Reward')]"), 25, 250); 
			//logger.info("log endreward",endRewardButton);
			System.out.println("End buttons: " + endRewardButtons.size());
            WebElement endRewardButton = endRewardButtons.get(0);
			if (endRewardButton == null)
				throw new Exception("Cannot find End Reward button.");
			mouseClick(driver, endRewardButton);
			System.out.println("Clicked on the End Reward button");
			
			waitUntilExists(driver, By.className("sweet-alert.showSweetAlert.visible"), 20, 250);
			WebElement okEndRewardButton = waitUntilExists(driver, By.className("confirm"), 20, 250);
			if (okEndRewardButton == null)
				throw new Exception("Could not find OK button to end the reward.");
			mouseClick(driver, okEndRewardButton);
			System.out.println("Clicked on the OK button");

			//driver.navigate().refresh();

			if (waitUntilNotExists(driver, By.xpath("//span[contains(.,'$5.00')]"), 80, 250) == false)
				throw new Exception("Unable to verify successful ending of the reward.");
			System.out.println("$5 reward successfully deleted");
			
			onSuccess(resultList);
			return results.done(true);

		} catch (Exception e) {
			results.error(e, e.getMessage());
		}
		return results.done(false);
	}

}
